-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema routing
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema routing
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `routing` DEFAULT CHARACTER SET latin1 ;
USE `routing` ;

-- -----------------------------------------------------
-- Table `routing`.`point`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `routing`.`point` (
  `idpoint` INT(11) NOT NULL AUTO_INCREMENT,
  `lat` DOUBLE NOT NULL,
  `lng` DOUBLE NOT NULL,
  PRIMARY KEY (`idpoint`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `routing`.`path`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `routing`.`path` (
  `idpath` INT(11) NOT NULL,
  `start` INT(11) NOT NULL,
  `end` INT(11) NOT NULL,
  PRIMARY KEY (`idpath`),
  INDEX `fk_start_idx` (`start` ASC),
  INDEX `fk_end_idx` (`end` ASC),
  CONSTRAINT `fk_line_point`
    FOREIGN KEY (`start`)
    REFERENCES `routing`.`point` (`idpoint`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_line_point1`
    FOREIGN KEY (`end`)
    REFERENCES `routing`.`point` (`idpoint`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `routing`.`linetype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `routing`.`linetype` (
  `idlinetype` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idlinetype`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `routing`.`line`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `routing`.`line` (
  `idline` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `idlinetype` INT NOT NULL,
  `linecolor` VARCHAR(9) NULL DEFAULT '#000000',
  `enabled` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idline`),
  INDEX `fk_line_linetype1_idx` (`idlinetype` ASC),
  CONSTRAINT `fk_line_linetype`
    FOREIGN KEY (`idlinetype`)
    REFERENCES `routing`.`linetype` (`idlinetype`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `routing`.`interchange`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `routing`.`interchange` (
  `idinterchange` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`idinterchange`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `routing`.`linepoint`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `routing`.`linepoint` (
  `idline` INT(11) NOT NULL,
  `idpoint` INT(11) NOT NULL,
  `sequence` INT(11) NOT NULL,
  `keep` TINYINT(1) NOT NULL DEFAULT 1,
  `stop` TINYINT(1) NOT NULL DEFAULT 0,
  `idinterchange` INT NULL,
  PRIMARY KEY (`idline`, `idpoint`),
  INDEX `fk_line_has_point_point1_idx` (`idpoint` ASC),
  INDEX `fk_line_has_point_line1_idx` (`idline` ASC),
  INDEX `fk_linepoint_interchange1_idx` (`idinterchange` ASC),
  CONSTRAINT `fk_line_has_point_line1`
    FOREIGN KEY (`idline`)
    REFERENCES `routing`.`line` (`idline`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_line_has_point_point1`
    FOREIGN KEY (`idpoint`)
    REFERENCES `routing`.`point` (`idpoint`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_linepoint_interchange1`
    FOREIGN KEY (`idinterchange`)
    REFERENCES `routing`.`interchange` (`idinterchange`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
