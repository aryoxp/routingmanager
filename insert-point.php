<?php

//var_dump($_POST);


$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}
//echo "A";
//exit;

$sql = "INSERT IGNORE INTO point (lat, lng) VALUES ";

$psql = array();
foreach($_POST['newPoints'] as $p) {
	$psql[] = "('" . $p['lat'] . "', '".$p['lng']."')";
}
$sql .= implode(", ", $psql);

if ($mysqli->query($sql) === TRUE) {
    $result = new stdClass;
	$result->status = true;
	$result->message = "point(s) Successfully inserted into database.";
	
	header('content-type:application/json');
	echo json_encode($result);
} else {
	$result = new stdClass;
	$result->status = false;
	$result->message = "Query error: ". $mysqli->error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}