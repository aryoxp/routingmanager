<?php

$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}



$linePoints = json_decode($_POST['linePoints']);
//var_dump($linePoints);exit;

$success = true;

$sql = "BEGIN";

$mysqli->query($sql);
	
$sql = "UPDATE linepoint SET keep = 0 WHERE idline = '".$_POST['idline']."'";

if ($mysqli->query($sql) === TRUE) {

	foreach($linePoints as $p) {

		$sql = "INSERT INTO point (idpoint, lat, lng) VALUES ";
		$sql .= "(".(empty($p->idpoint)?'NULL':"'".$p->idpoint."'").", '" . $p->lat . "', '".$p->lng."') ";
		$sql .= "ON DUPLICATE KEY UPDATE lat = VALUES(lat), lng=VALUES(lng) ";

		//var_dump($sql);//exit;
		$idpoint = (empty($p->idpoint)?'LAST_INSERT_ID()':"'".$p->idpoint."'");

		if ($mysqli->query($sql) === TRUE) {
			$sql = "INSERT INTO linepoint (idline, idpoint, sequence) VALUES ";
			$sql .= "(".$p->idline.", $idpoint, '".$p->sequence."') ";
			$sql .= "ON DUPLICATE KEY UPDATE sequence=VALUES(sequence), keep=1 ";
			//var_dump($sql);//exit;
			if($mysqli->query($sql) === TRUE) {
				$result = new stdClass;
				$result->status = true;
				$result->message = "point(s) Successfully saved into database.";
			} else {
				$success = false;
			}
		} else {
			$success = false;
		}


	} 
	
	//$sql = "DELETE FROM linepoint WHERE idline = '".$_POST['idline']."' AND keep = 0";
	
	//$mysqli->query($sql);
	
	$sql = "DELETE FROM point WHERE idpoint NOT IN (
			SELECT idpoint FROM linepoint WHERE keep = 1
			UNION
			SELECT start AS idpoint FROM path
			UNION
			SELECT end AS idpoint FROM path
	) ";
	
	$mysqli->query($sql);

} else {
	$success = false;
}

if($success)
{
	$mysqli->query('COMMIT');
	$mysqli->query($sql);

	//header('content-type:application/json');
	echo json_encode($result);
	exit();
}

$mysqli->query('ROLLBACK');

$result = new stdClass;
$result->status = false;
$result->message = "Query error: ". $mysqli->error;
//header('content-type:application/json');
echo json_encode($result);
exit();