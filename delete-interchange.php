<?php
$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}

$sql = "DELETE FROM interchange WHERE idinterchange = '".$_POST['idinterchange']."'";
if ($mysqli->query($sql) === TRUE) {
    $result = new stdClass;
	$result->status = true;
	$result->message = "Interchange successfully deleted.";
	
	header('content-type:application/json');
	echo json_encode($result);
} else {
	$result = new stdClass;
	$result->status = false;
	$result->message = "Query error: ". $mysqli->error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}