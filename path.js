$(function(){
	
	var url = "points.php"; //alert(url);
	var markerIcon = "orange_8x8.png";
	var markerSelectedIcon = "orange-square_8x8.png";
	var markers = [];
	
	var newLine = [];
	
	var poly;

	function clearPoints() {
		markers.forEach(function(m){
			m.setMap(null);	
		});
		markers.splice(0, markers.length);
	}
	
	function loadPoints() {
		$.get(url, function(result){
			//console.log(data);
			//console.log(map);
			console.log(result.length + " points loaded");
			
			result.forEach(function(d){
				
				pos = new google.maps.LatLng(d.lat, d.lng);
				var marker = new google.maps.Marker({
					position: pos,
					icon: markerIcon,
					map: map,
					modified: false,
					selected: false,
					selectedToSave: false,
					id: d.idpoint
				});
				marker.addListener('click', function() {
				  //map.setZoom(18);
				  //map.setCenter(marker.getPosition());
				  //console.log(this.icon);
				  if(this.selected) {
					  this.selected = false;
					  this.setIcon(markerIcon);
				  } else {
					  this.selected = true;
					  this.setIcon(markerSelectedIcon);
				  }
				});
				marker.addListener('dblclick', function (e) { 
				   
				   var id = markers.indexOf(this);
				   console.log(id);
				   if (id > -1)
						markers.splice(id, 1);
				   console.log(markers.length);
				   this.setMap(null);
				});
				markers.push(marker);
				
			});
			
			//console.log(markers);
			
		}, 'json');
	
	}
	
	initMap();
	clearPoints();
	//loadPoints();
	
	// Handles click events on a map, and adds a new point to the Polyline.
	function addLatLng(event) {
		var path = poly.getPath();
		// Because path is an MVCArray, we can simply append a new coordinate
		// and it will automatically appear.
		path.push(event.latLng);
	}
	
	function deleteNode(event) {
		if (event.vertex != null) {
			poly.getPath().removeAt(event.vertex);
		}
	}

	poly = new google.maps.Polyline({
	  strokeColor: '#55FF00',
	  strokeOpacity: 0.5,
	  strokeWeight: 5,
	  editable: true
	});
	
	var vertex;
	var idpoint;
	function polyClick(event) {
		console.log('Poly Click');
		console.log(event);
		console.log(event.latLng);
		vertex = event.vertex;
		idpoint = event.latLng.idpoint;
	}
	
	google.maps.event.addListener(poly, 'rightclick', deleteNode);
	google.maps.event.addListener(poly, 'mousedown', polyClick);
	
	poly.setMap(map);

	// Add a listener for the click event
	map.addListener('click', addLatLng);
	
	
	var urlLine = "lines.php";
	$.get(urlLine, function(data){
		$('#select-line').empty();
		data.forEach(function(l){
			$('#select-line').append('<option data-linecolor="'+l.linecolor+'" value="'+l.idline+'">'+l.name+'</option>');
		});
	}, 'json');
	
	function loadLine() {
		$.get(urlLoadLine, {'line': $('#select-line').val()}, function(data){
			//console.log(data);
			path = poly.getPath();
			path.clear();
			data.forEach(function(p){
				var pt = new google.maps.LatLng(p.lat, p.lng);
				pt.idpoint = p.idpoint;
				path.push(pt);
			});
			
			poly.setOptions({
				strokeColor: $('#select-line').find(':selected').data('linecolor')
			});
			console.log(path);
		});
	}	
	
	
	var urlSaveLine = "save-line.php";
	var linePoints = [];
	
	function addPoint(element, index) {
		//console.log(element);
		//p = element.getAt(index);
		np = {
				idline: $('#select-line').val(),
				idpoint: element.idpoint,
				lat: element.lat(),
				lng: element.lng(),
				sequence: index
			}
		//console.log(np);
		linePoints.push(np);
	}
	
	$('.btn-save-line').click(function(){
		iziToast.info({
				title: 'Save Line',
				message: 'Saving line...',
			});
		linePoints.splice(0, linePoints.length);
		console.log(poly.getPath().getArray());
		poly.getPath().forEach(addPoint);
		var saveData = {
			'linePoints': JSON.stringify(linePoints),
			'idline': $('#select-line').val()
			};
		$.post(urlSaveLine, saveData, function(data){
			console.log(data);
			loadLine();
			iziToast.success({
				title: 'OK',
				message: 'Line saved.',
			});
		});
		
	});
	
	var urlLoadLine = "points.php";	
	$('.btn-load-line').click(function(){
		loadLine();
	});
	
	
	function setAt(event){
		//alert(idpoint);
		poly.getPath().getAt(event).idpoint = idpoint;
		poly.getPath().getAt(event).modified = true;
		console.log("SET: idpoint: " + idpoint);
		//console.log(poly.getPath());
	}
	google.maps.event.addListener(poly.getPath(), "set_at", setAt);
	
	/*
	function dragEnd(event){
		console.log("DRAG:" + poly.getPath().getAt(event));
	}
	
	function insertAt(event){
		console.log("INSERT:" + event);
	}
	
	function removeAt(event){
		console.log("REMOVE:" + event);
	}
	
	
	
	function click(event){
		console.log("CLICK:" + event);
	}
	*/
	//google.maps.event.addListener(poly.getPath(), "dragend", dragEnd);
	//google.maps.event.addListener(poly.getPath(), "insert_at", insertAt);
	//google.maps.event.addListener(poly.getPath(), "remove_at", removeAt);
	//
	//google.maps.event.addListener(poly.getPath(), "click", click);
	

	
	/*
	var insertPointUrl = "insert-point.php";
	$('.btn-save-new-points').click(function(){
		//console.log(newMarkers);
		var nm = [];
		newMarkers.forEach(function(m) {
			nm.push({
				lat: m.getPosition().lat(),
				lng: m.getPosition().lng()
			})
		});
		console.log(nm);
		$.post(insertPointUrl, {newPoints: nm}, function(data) {
			if(data.status) {
				newMarkers.forEach(function(nm) {
					nm.modified = false;
					nm.selected = false;
					nm.selectedToSave = false;
					nm.setIcon(markerIcon);
					markers.push(nm);
				});
				newMarkers.splice(0, newMarkers.length);
				
				clearPoints();
				loadPoints();
			}
		});
	});
	
	var savePointUrl = "save-point.php";
	$('.btn-save-selected-points').click(function(){
		//console.log(newMarkers);
		var sm = [];
		markers.forEach(function(m) {
			if(m.selected) {
				sm.push({
					id: m.id,
					lat: m.getPosition().lat(),
					lng: m.getPosition().lng()
				});
			}
		});
		console.log(sm);
		$.post(savePointUrl, {selectedPoints: sm}, function(data) {
			console.log(data);
			if(data.status) {
				markers.forEach(function(sm) {
					sm.modified = false;
					sm.selected = false;
					sm.selectedToSave = false;
					sm.setIcon(markerIcon);
				});
			}
		});
	});
	
	var deletePointUrl = "delete-point.php";
	$('.btn-delete-selected-points').click(function(){
		//console.log(newMarkers);
		
		var result = confirm('DELETE selected markers?');
		if(!result) return true;
		
		var sm = [];
		markers.forEach(function(m) {
			if(m.selected) {
				sm.push({
					id: m.id
				});
			}
		});
		console.log(sm);
		$.post(deletePointUrl, {selectedPoints: sm}, function(data) {
			console.log(data);
			if(data.status) {
				
				markers.forEach(function(sm) {
					if(sm.selected) {
						var id = markers.indexOf(sm);
						console.log(id);
						if (id > -1)
							markers.splice(id, 1);
						console.log(markers.length);
						sm.setMap(null);
					}
				});
			}
		});
	});
	
	*/
	
});