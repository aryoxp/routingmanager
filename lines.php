<?php

$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}

$sql = "SELECT idline, CONCAT(name, '-', direction) AS name,
        linecolor, idlinetype FROM line WHERE enabled = 1
        ORDER by FIELD(direction, 'I'), name";

$lines = array();
if ($result = $mysqli->query($sql)) {
	while($row = $result->fetch_object())
		$lines[] = $row;
}

header('content-type:application/json');
echo json_encode($lines);
exit();
