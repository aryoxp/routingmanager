<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="style.css" />
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/iziToast.min.css">

	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/iziToast.min.js"></script>
	
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCUBhkYZItMyK0CloDA2sJuzybGI9v0Eb4"></script>
	
</head>
<body>