<?php
include "head.php";
?>
<script src="path.js"></script>

    <div id="map"></div>
	
	<script>
	
	var map;
	var markerNewIcon = "green_8x8.png";
	var newMarkers = [];
	
	function initMap() {
        var myLatLng = {lat: -25.363, lng: 131.044};
		var malang = {
              lat: -7.982379,
              lng: 112.630321
            };
        // Create a map object and specify the DOM element for display.
        map = new google.maps.Map(document.getElementById('map'), {
          center: malang,
          zoom: 13,
		  clickableIcons: false,
		  styles: [{ featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]}]
        });

        // Create a marker and set its position.
        var marker = new google.maps.Marker({
          map: map,
          position: myLatLng,
          title: 'Hello World!'
        });
		
		infoWindow = new google.maps.InfoWindow;
		
		// Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            //infoWindow.open(map);
			
			var malang = {
              lat: -7.982379,
              lng: 112.630321
            };
			map.setCenter(malang);
			map.setZoom(13);
			
            //map.setCenter(pos);
			//map.setZoom(18);
			//marker.setPosition( new google.maps.LatLng( 0, 0 ) );
			//marker.setPosition(malang);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }

    }
		
	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(browserHasGeolocation ?
							  'Error: The Geolocation service failed.' :
							  'Error: Your browser doesn\'t support geolocation.');
		//infoWindow.open(map);		
			var malang = {
              lat: -7.982379,
              lng: 112.630321
            };
			map.setCenter(malang);
			map.setZoom(13);
			
            //map.setCenter(pos);
			//map.setZoom(18);
			//marker.setPosition( new google.maps.LatLng( 0, 0 ) );
			//marker.setPosition(malang);
		
	}
	</script>
	
	<hr>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Add/Remove Path</h2>
				<hr>
				<div class="btn-group" role="group">
					<select id="select-line" class="custom-select"></select>
					<button class="btn btn-primary btn-load-line">Load</button>
					<button class="btn btn-success btn-save-line">Save Line</button>
				</div>
				<hr>
				
				<button class="btn btn-primary btn-save-selected-points">Save selected paths</button>
				<button class="btn btn-danger btn-delete-selected-points">DELETE selected paths</button>
			</div>
		</div>
	</div>
		
		
<?php
include "foot.php";