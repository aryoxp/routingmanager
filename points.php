<?php

$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}

$sql = "SELECT p.idpoint, p.lat, p.lng, l.sequence, l.stop, ln.linecolor FROM linepoint l LEFT JOIN point p ON l.idpoint = p.idpoint LEFT JOIN line ln ON ln.idline = l.idline ";
if(isset($_GET['line']))
	$sql .= "WHERE l.idpoint IN (SELECT lp.idpoint FROM linepoint lp WHERE lp.idline = '".$_GET['line']."') ";
$sql .= "ORDER BY l.sequence ASC";
//echo $sql;
$points = array();
if ($result = $mysqli->query($sql)) {
	while($row = $result->fetch_object())
		$points[] = $row;
}

header('content-type:application/json');
echo json_encode($points);
exit();
