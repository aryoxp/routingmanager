$(function(){
	
	var lines = [];
	var poly;

	/*
	var pointImage = "circle-8.png";
	var pointIcon = new google.maps.MarkerImage(
				pointImage,
                new google.maps.Size(8, 8),
                new google.maps.Point(0, 0),
                new google.maps.Point(4, 4));
	*/
	
	var pointIcon = {  
        path: "M-4,0a4,4 0 1,0 8,0a4,4 0 1,0 -8,0",
        fillColor: '#FF0000',
        fillOpacity: .5,
        anchor: new google.maps.Point(0,0),
        strokeWeight: 0,
	}
				
				
	var stopImage = "square-10.png";
	var stopIcon = new google.maps.MarkerImage(
				stopImage,
                new google.maps.Size(10, 10),
                new google.maps.Point(0, 0),
                new google.maps.Point(5, 5));
				
	initMap();
	
	
	
	var urlLine = "lines.php";
	$.get(urlLine, function(data){
		$('#select-line').empty();
		data.forEach(function(l){
			$('#select-line').append('<div class="col-md-2 form-check"><label class="form-check-label"><input type="checkbox" data-linecolor="'+l.linecolor+'" data-idline="'+l.idline+'" data-name="'+l.name+'" class="form-check-input cb-line" /> '+l.name+'</label></div>');
		});
	}, 'json');
	
	
	function addLine(line) {
		
		markerSet = [];
		
		poly = new google.maps.Polyline({
		  strokeColor: line.linecolor,
		  strokeOpacity: 0.5,
		  strokeWeight: 5,
		  idline: line.idline
		});
		
		poly.setMap(map);
		
		line.points.forEach(function(p){
			var path = new google.maps.LatLng(p.lat, p.lng);
			path.idpoint = p.idpoint;
			path.stop = p.stop;
			//console.log(path);
			poly.getPath().push(path);
			pointIcon.fillColor = p.linecolor;
			icon = (p.stop == "1") ? stopIcon : pointIcon;
			var marker = new google.maps.Marker({
				position: path,
				map: map,
				icon: icon,
				idline: line.idline,
				idpoint: p.idpoint,
				stop: p.stop,
				title: line.idline + ":" + line.name
			});
			marker.addListener('click', function() {
				//console.log(this);
				if(this.stop == "0") {
					this.setIcon(stopIcon);
					this.stop = "1";
				} else {
					this.setIcon(pointIcon);
					this.stop = "0";
				}
				var urlSetStop = "set-stop.php";
				var param = {
					stop: this.stop,
					idpoint: p.idpoint,
					idline: line.idline
					}
				$.post(urlSetStop, param, function(data){
					console.log(data);
				});
			});
			//console.log(marker);
			path.marker = marker;
			markerSet.push(marker);
		});		
		poly.markerSet = markerSet;
		lines.push(poly);	
	}
	$('#select-line').on('change', '.cb-line', function(){
		console.log("Checkbox: " + $(this).is(':checked'));
		var cb = $(this);
		if(cb.is(':checked')) {
			var getLineUrl = "points.php";
			$.get(getLineUrl, {line:cb.data('idline')}, function(data){
				console.log(data);
				var line = {
					idline: cb.data('idline'),
					name: cb.data('name'),
					linecolor: cb.data('linecolor'),
					points: data
				}
				console.log(line);
				addLine(line);
			});
		} else {
			lines.forEach(function(line){
				if(line.idline == cb.data('idline')) {
					line.markerSet.forEach(function(m){
						m.setMap(null);
					});
					line.markerSet.splice(0, line.markerSet.length);
					console.log(line);
					line.setMap(null);
				}
			});
		}
	});
	
	function loadLine() {
		$.get(urlLoadLine, {'line': $('#select-line').val()}, function(data){
			//console.log(data);
			path = poly.getPath();
			path.clear();
			data.forEach(function(p){
				var pt = new google.maps.LatLng(p.lat, p.lng);
				pt.idpoint = p.idpoint;
				path.push(pt);
			});
			
			poly.setOptions({
				strokeColor: $('#select-line').find(':selected').data('linecolor')
			});
			console.log(path);
		});
	}
	var urlLoadLine = "points.php";	
	$('.btn-load-line').click(function(){
		loadLine();
	});
	
	
	/*
	
	
	
	
	var urlSaveLine = "save-line.php";
	var linePoints = [];
	
	function addPoint(element, index) {
		//console.log(element);
		//p = element.getAt(index);
		np = {
				idline: $('#select-line').val(),
				idpoint: element.idpoint,
				lat: element.lat(),
				lng: element.lng(),
				sequence: index
			}
		//console.log(np);
		linePoints.push(np);
	}
	
	$('.btn-save-line').click(function(){
		iziToast.info({
				title: 'Save Line',
				message: 'Saving line...',
			});
		linePoints.splice(0, linePoints.length);
		console.log(poly.getPath().getArray());
		poly.getPath().forEach(addPoint);
		var saveData = {
			'linePoints': linePoints,
			'idline': $('#select-line').val()
			};
		$.post(urlSaveLine, saveData, function(data){
			console.log(data);
			loadLine();
			iziToast.success({
				title: 'OK',
				message: 'Line saved.',
			});
		});
		
	});
	
	
	
	function setAt(event){
		//alert(idpoint);
		poly.getPath().getAt(event).idpoint = idpoint;
		poly.getPath().getAt(event).modified = true;
		console.log("SET: idpoint: " + idpoint);
		//console.log(poly.getPath());
	}
	google.maps.event.addListener(poly.getPath(), "set_at", setAt);
	*/
	
});