$(function(){
	
	var url = "points.php"; //alert(url);
	var markerIcon = "orange_8x8.png";
	var markerSelectedIcon = "orange-square_8x8.png";
	var markers = [];


    var pointIcon = {  
        path: "M-4,0a4,4 0 1,0 8,0a4,4 0 1,0 -8,0",
        fillColor: '#FF0000',
        fillOpacity: .6,
        anchor: new google.maps.Point(0,0),
        strokeWeight: 0,
	}
	
	function clearPoints() {
		markers.forEach(function(m){
			m.setMap(null);	
		});
		markers.splice(0, markers.length);
	}
	
	function loadPoints() {
		$.get(url, function(result){
			//console.log(data);
			//console.log(map);
			console.log(result.length + " points loaded");
			
			result.forEach(function(d){
				
				pos = new google.maps.LatLng(d.lat, d.lng);
				var marker = new google.maps.Marker({
					position: pos,
					icon: pointIcon,
					map: map,
					draggable: true,
					modified: false,
					selected: false,
					selectedToSave: false,
					id: d.idpoint
				});
				marker.addListener('click', function() {
				  //map.setZoom(18);
				  //map.setCenter(marker.getPosition());
				  //console.log(this.icon);
				  if(this.selected) {
					  this.selected = false;
					  this.setIcon(markerIcon);
				  } else {
					  this.selected = true;
					  this.setIcon(markerSelectedIcon);
				  }
				});
				marker.addListener('dblclick', function (e) { 
				   
				   var id = markers.indexOf(this);
				   console.log(id);
				   if (id > -1)
						markers.splice(id, 1);
				   console.log(markers.length);
				   this.setMap(null);
				});
				markers.push(marker);
				
			});
			
			//console.log(markers);
			
		}, 'json');
	
	}
	
	initMap();
	clearPoints();
	loadPoints();
	
	var insertPointUrl = "insert-point.php";
	$('.btn-save-new-points').click(function(){
		//console.log(newMarkers);
		var nm = [];
		newMarkers.forEach(function(m) {
			nm.push({
				lat: m.getPosition().lat(),
				lng: m.getPosition().lng()
			})
		});
		console.log(nm);
		$.post(insertPointUrl, {newPoints: nm}, function(data) {
			if(data.status) {
				newMarkers.forEach(function(nm) {
					nm.modified = false;
					nm.selected = false;
					nm.selectedToSave = false;
					nm.setIcon(markerIcon);
					markers.push(nm);
				});
				newMarkers.splice(0, newMarkers.length);
				
				clearPoints();
				loadPoints();
			}
		});
	});
	
	var savePointUrl = "save-point.php";
	$('.btn-save-selected-points').click(function(){
		//console.log(newMarkers);
		var sm = [];
		markers.forEach(function(m) {
			if(m.selected) {
				sm.push({
					id: m.id,
					lat: m.getPosition().lat(),
					lng: m.getPosition().lng()
				});
			}
		});
		console.log(sm);
		$.post(savePointUrl, {selectedPoints: sm}, function(data) {
			console.log(data);
			if(data.status) {
				markers.forEach(function(sm) {
					sm.modified = false;
					sm.selected = false;
					sm.selectedToSave = false;
					sm.setIcon(markerIcon);
				});
			}
		});
	});
	
	var deletePointUrl = "delete-point.php";
	$('.btn-delete-selected-points').click(function(){
		//console.log(newMarkers);
		
		var result = confirm('DELETE selected markers?');
		if(!result) return true;
		
		var sm = [];
		markers.forEach(function(m) {
			if(m.selected) {
				sm.push({
					id: m.id
				});
			}
		});
		console.log(sm);
		$.post(deletePointUrl, {selectedPoints: sm}, function(data) {
			console.log(data);
			if(data.status) {
				
				markers.forEach(function(sm) {
					if(sm.selected) {
						var id = markers.indexOf(sm);
						console.log(id);
						if (id > -1)
							markers.splice(id, 1);
						console.log(markers.length);
						sm.setMap(null);
					}
				});
			}
		});
	});
	
});