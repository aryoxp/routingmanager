<?php

$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}

$sql = "SELECT i.idinterchange, i.name, GROUP_CONCAT(l.idpoint) AS idpoints
FROM interchange i LEFT JOIN linepoint l ON l.idinterchange = i.idinterchange
GROUP BY i.idinterchange";
//echo $sql;
$points = array();
if ($result = $mysqli->query($sql)) {
	while($row = $result->fetch_object())
		$points[] = $row;
}

header('content-type:application/json');
echo json_encode($points);
exit();
