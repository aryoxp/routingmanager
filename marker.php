<?php
include "head.php";
?>
<script src="points.js"></script>

    <div id="map"></div>
	
	<script>
	
	var map;
	var markerNewIcon = "green_8x8.png";
	

	
	var newMarkers = [];
	
	function initMap() {
        var myLatLng = {lat: -25.363, lng: 131.044};

        // Create a map object and specify the DOM element for display.
        map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 4,
		  clickableIcons: false,
		  styles: [{ featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]}]
        });

        // Create a marker and set its position.
        var marker = new google.maps.Marker({
          map: map,
          position: myLatLng,
          title: 'Hello World!'
        });
		
		infoWindow = new google.maps.InfoWindow;
		
		// Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            //infoWindow.open(map);
			var malang = {
              lat: -7.982379,
              lng: 112.630321
            };
			map.setCenter(malang);
			map.setZoom(13);
			
            //map.setCenter(pos);
			//map.setZoom(18);
			//marker.setPosition( new google.maps.LatLng( 0, 0 ) );
			//marker.setPosition(malang);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
		
		map.addListener('click', function(e) {
			placeMarker(e.latLng, map);
		});

    }

	function placeMarker(position, map) {
		var marker = new google.maps.Marker({
			position: position,
			map: map,
			icon: markerNewIcon,
			draggable: true
		});
		
		marker.addListener('dblclick', function (e) {
		   var id = newMarkers.indexOf(this);
		   console.log(id);
		   if (id > -1)
				newMarkers.splice(id, 1);
		   //console.log(newMarkers.length);
		   this.setMap(null);
		});
			
		newMarkers.push(marker);
		//console.log(newMarkers.length);
	}
		
	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(browserHasGeolocation ?
							  'Error: The Geolocation service failed.' :
							  'Error: Your browser doesn\'t support geolocation.');
		infoWindow.open(map);
			var malang = {
              lat: -7.982379,
              lng: 112.630321
            };
			map.setCenter(malang);
			map.setZoom(13);
			
            //map.setCenter(pos);
			//map.setZoom(18);
			//marker.setPosition( new google.maps.LatLng( 0, 0 ) );
			//marker.setPosition(malang);
	}
	</script>
	
	<hr>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2>Add/Remove Point</h2>
				<hr>
				<button class="btn btn-success btn-save-new-points">Save new points</button>
				<button class="btn btn-primary btn-save-selected-points">Save selected points</button>
				<button class="btn btn-danger btn-delete-selected-points">DELETE selected points</button>
			</div>
		</div>
	</div>
		
		
<?php
include "foot.php";