$(function(){
	
	var lines = [];
	var interchanges = [];
	var poly;
	var interchangeLines = [];

	/*
	var pointImage = "circle-8.png";
	var pointIcon = new google.maps.MarkerImage(
				pointImage,
                new google.maps.Size(8, 8),
                new google.maps.Point(0, 0),
                new google.maps.Point(4, 4));
	*/
	
	var stopImage = "square-10.png";
	var stopIcon = new google.maps.MarkerImage(
				stopImage,
                new google.maps.Size(10, 10),
                new google.maps.Point(0, 0),
                new google.maps.Point(5, 5));

	var stopSelectedImage = "square-selected-10.png";
	var stopSelectedIcon = new google.maps.MarkerImage(
				stopSelectedImage,
                new google.maps.Size(10, 10),
                new google.maps.Point(0, 0),
                new google.maps.Point(5, 5));
				
	var selectedStops = [];
				
	initMap();

	var interchangeIcon = {  
        path: "M-18,0a18,18 0 1,0 36,0a18,18 0 1,0 -36,0",
        fillColor: '#FF0000',
        fillOpacity: .6,
        anchor: new google.maps.Point(0,0),
        strokeWeight: 4,
		strokeColor: '#000000'
	}
	
	var urlLine = "lines.php";
	$.get(urlLine, function(data){
		$('#select-line').empty();
		data.forEach(function(l){
			$('#select-line').append('<div class="col-md-2 form-check"><label class="form-check-label"><input type="checkbox" data-linecolor="'+l.linecolor+'" data-idline="'+l.idline+'" data-name="'+l.name+'" class="form-check-input cb-line" /> '+l.name+'</label></div>');
		});
	}, 'json');
	
	function addMarker(marker) {
		selectedStops.push(marker);
		$('#selected-stops').empty();
		selectedStops.forEach(function(marker){
			$('#selected-stops').append('<div>'+marker.idpoint+': '+marker.position.lat()+', '+marker.position.lng()+'</div>');
		});
	}
	
	function removeMarker(marker) {
		index = selectedStops.indexOf(marker);
		selectedStops.splice(index, 1);
		$('#selected-stops').empty();
		selectedStops.forEach(function(marker){
			//$('#selected-stops').append('<div>'+marker.idpoint+': '+marker.position.lat()+', '+marker.position.lng()+'</div>');
		});
	}
	
	var markers = [];
	
	function addLine(line) {
		
		markerSet = [];
		
		
		poly = new google.maps.Polyline({
		  strokeColor: line.linecolor,
		  strokeOpacity: 0.5,
		  strokeWeight: 5,
		  idline: line.idline
		});
		
		poly.setMap(map);
		
		line.points.forEach(function(p){

			var path = new google.maps.LatLng(p.lat, p.lng);
			path.idpoint = p.idpoint;
			path.stop = p.stop;
			//console.log(path);
			poly.getPath().push(path);
			if(p.stop == "1") {
				var marker = new google.maps.Marker({
					position: path,
					map: map,
					icon: stopIcon,
					idline: line.idline,
					idpoint: p.idpoint,
					stop: p.stop,
					title: line.idline + ":" + line.name,
					selected: false
				});
				marker.addListener('click', function() {
					//console.log(this);
					if(this.selected) {
						this.setIcon(stopIcon);
						this.selected = false;
						removeMarker(this);
					} else {
						this.setIcon(stopSelectedIcon);
						this.selected = true;
						addMarker(this);
					}
				});
				//console.log(marker);
				path.marker = marker;
				markerSet.push(marker);
				markers.push(marker);
			} else { path.marker = null; }
		});		
		poly.markerSet = markerSet;
		lines.push(poly);
		
		//return;
		
		
		interchanges.forEach(function(i){
			var ltLgs = [];
			var interchangeMarkerSet = [];
			i.points.forEach(function(p){
				markers.forEach(function(m){
					//console.log(p);
					//console.log(m);
					if(p == m.idpoint) {
						ltLgs.push(m.position);
						interchangeMarkerSet.push(m);
					}
				});
				
			});
			//console.log(i.idinterchange);
			var interchangeLine = new google.maps.Polyline({
				idinterchange: i.idinterchange,
				path: ltLgs,
				geodesic: true,
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 14,
				markers: interchangeMarkerSet
			});
			//console.log(interchangeLine);
			interchangeLine.setMap(map);
			interchangeLines.push(interchangeLine);
			
			google.maps.event.addListener(interchangeLine, 'click', function(){
				var idInterchange = this.idinterchange;
				$('#list-interchange').empty();
				$('#list-interchange').append('<strong>ID Interchange: '+idInterchange+'</strong>');
				this.markers.forEach(function(m){
					$('#list-interchange').append('<div>'
						+m.idpoint+': '+m.position.lat()+', '+m.position.lng()+'</div>');
				});
				$('#list-interchange').append('<hr>');
				$('#list-interchange').append('<button data-idinterchange="'
					+idInterchange
					+'" class="btn btn-danger btn-delete-interchange">Delete Interchange</button>');
			});
							
			
		});
	}

	$('#list-interchange').on('click', '.btn-delete-interchange', function() {
		console.log($(this).data('idinterchange'));
		var idInterchange = $(this).data('idinterchange');
		var deleteInterchangeUrl = 'delete-interchange.php';
		$.post(deleteInterchangeUrl, {'idinterchange': idInterchange}, function(result){
			console.log(result);
			if(result.status) {

				idx=0;
				interchanges.forEach(function(i){
					if(i.idinterchange == idInterchange) {
						interchanges.splice(idx,1);
					}
					idx++;
				});
				
				idx=0;
				interchangeLines.forEach(function(il){
					if(il.idinterchange == idInterchange) {
						interchangeLines.splice(idx, 1);
						il.setMap(null);
					}
					idx++;
				});

			}
		}, 'json');
	});

	$('#select-line').on('change', '.cb-line', function(){
		//console.log("Checkbox: " + $(this).is(':checked'));
		var cb = $(this);
		if(cb.is(':checked')) {
			var getLineUrl = "get-stops.php";
			$.get(getLineUrl, {line:cb.data('idline')}, function(data){
				//console.log(data);
				var line = {
					idline: cb.data('idline'),
					name: cb.data('name'),
					linecolor: cb.data('linecolor'),
					points: data
				}
				console.log(line);
				addLine(line);
			});
		} else {
			lines.forEach(function(line){
				if(line.idline == cb.data('idline')) {
					line.markerSet.forEach(function(m){
						m.setMap(null);
					});
					line.markerSet.splice(0, line.markerSet.length);
					console.log(line);
					line.setMap(null);
				}
			});
		}
		
		
	});
	
	function loadLine() {
		$.get(urlLoadLine, {'line': $('#select-line').val()}, function(data){
			//console.log(data);
			path = poly.getPath();
			path.clear();
			data.forEach(function(p){
				var pt = new google.maps.LatLng(p.lat, p.lng);
				pt.idpoint = p.idpoint;
				path.push(pt);
			});
			
			poly.setOptions({
				strokeColor: $('#select-line').find(':selected').data('linecolor')
			});
			console.log(path);
		});
	}
	var urlLoadLine = "points.php";	
	$('.btn-load-line').click(function(){
		loadLine();
	});
	
	
	$('.btn-new-interchange').click(function(){
		var stops = [];
		var latLngs = [];
		selectedStops.forEach(function(s){
			stops.push({
				idpoint: s.idpoint,
				lat: s.position.lat(),
				lng: s.position.lng()
			});
			latLngs.push(new google.maps.LatLng(
				s.position.lat(), 
				s.position.lng()
			));
		});
		var data = {
			stops: stops
		};
	
		var newInterchangeUrl = "new-interchange.php";
		$.post(newInterchangeUrl, data, function(result){
			console.log(result);
			var sumLat = 0;
			var sumLng = 0;
			stops.forEach(function(s){
				sumLat += s.lat;
				sumLng += s.lng;
			});
			console.log(latLngs);
			var interchangeLine = new google.maps.Polyline({
				path: latLngs,
				geodesic: true,
				strokeColor: '#FF0000',
				strokeOpacity: 1.0,
				strokeWeight: 14
			});

			interchangeLine.setMap(map);
			
			selectedStops.splice(0, selectedStops.length);
			$('#selected-stops').empty();
			
		});
		
	});
	
	
	
	
	function loadInterchange(){
		var getInterchangeUrl = "get-interchange.php";
		$.get(getInterchangeUrl, function(result){
			console.log(result);
		
			$('#list-interchange').empty();
			result.forEach(function(i){
			
				row = '<div>'+i.idinterchange+': '+i.idpoints+' <button class="btn btn-sm btn-danger btn-delete-interchange">Delete</button></div>';
				points = [];
				if(i.idpoints != null)
					points = i.idpoints.split(",");
				i.points = points;
				//console.log(points);
				//$('#list-interchange').append(row);
				interchanges.push(i);
				
			});
			

		});
	}
	loadInterchange();
	
	$('#btn-load-interchange').click(function(){
		loadInterchange();
	});
		
});