<?php

$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}

//echo "A";
//exit;

$sql = "INSERT INTO interchange (name) VALUES (NULL)";
$mysqli->query($sql);
$idinterchange = $mysqli->insert_id;

$psql = array();
foreach($_POST['stops'] as $p) {
	$psql[] = $p['idpoint'];
}
$sql = "UPDATE linepoint SET idinterchange = '$idinterchange' WHERE idpoint IN ";
$sql .= "( " . implode(", ", $psql) . " )";

//var_dump($sql);exit;

if ($mysqli->query($sql) === TRUE) {
    $result = new stdClass;
	$result->status = true;
	$result->message = "point(s) Successfully added into interchange.";
	
	header('content-type:application/json');
	echo json_encode($result);
} else {
	$result = new stdClass;
	$result->status = false;
	$result->message = "Query error: ". $mysqli->error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}