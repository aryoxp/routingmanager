<?php

$mysqli = new mysqli("localhost", "root", "", "routing");
//var_dump($mysqli);
/* check connection */
if ($mysqli->connect_errno) {
    $result = new stdClass;
	$result->status = false;
	$result->message = "Connect failed: ". $mysqli->connect_error;
	header('content-type:application/json');
	echo json_encode($result);
    exit();
}

$sql = "SELECT lp.idpoint AS id, p.lat, p.lng, lp.idline AS l, lp.stop AS st,
        ln.name AS n, ln.direction AS d, ln.linecolor AS c, lp.sequence AS s,
        (
			SELECT l.idpoint
				FROM linepoint l WHERE l.idline = lp.idline AND l.sequence = lp.sequence+1
			) AS a,
			(
			SELECT GROUP_CONCAT(l.idpoint)
                FROM linepoint l WHERE l.idinterchange = lp.idinterchange AND l.idpoint <> lp.idpoint AND l.idinterchange IS NOT NULL GROUP BY lp.idpoint
			) AS i
		FROM linepoint lp LEFT JOIN point p ON lp.idpoint = p.idpoint
		LEFT JOIN line ln ON lp.idline = ln.idline
		ORDER BY lp.idpoint ASC";
//echo $sql;
$points = array();
if ($result = $mysqli->query($sql)) {
	while($row = $result->fetch_object())
		$points[] = $row;
}
if($mysqli->error) echo $mysqli->error;
header('content-type:application/json');
echo json_encode($points);
exit();
