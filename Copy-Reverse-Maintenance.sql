-- Copy reverse line

CREATE TEMPORARY TABLE tAL AS (
SELECT 
'18' AS idline,
@row := @row+1 as sequence,
lp.stop,
lp.idinterchange,
p.lat,
p.lng,
p.lng + 0.0001 AS nlng
FROM linepoint lp LEFT JOIN line l ON l.idline = lp.idline
LEFT JOIN point p ON lp.idpoint = p.idpoint, (SELECT @row := -1) r 
WHERE l.idline = '2'
ORDER BY lp.sequence DESC
LIMIT 10000
);

INSERT INTO point (lat, lng) SELECT lat, nlng FROM tAL ORDER BY sequence;

INSERT INTO linepoint (idline, sequence, stop, idinterchange, idpoint)
(
	SELECT idline, sequence, stop, idinterchange,
    (
		SELECT pt.idpoint FROM point pt WHERE pt.lat = tAL.lat AND pt.lng = tAL.nlng ORDER BY idpoint DESC LIMIT 1 
    ) AS idpoint
    FROM tAL 
);

SELECT * FROM tAL;

DROP TEMPORARY TABLE tAL;

DELETE FROM point WHERE idpoint NOT IN (SELECT idpoint FROM linepoint);


-- Maintenance SQL

-- Hapus line

DELETE FROM linepoint WHERE idline = '16';
DELETE FROM point WHERE idpoint NOT IN (SELECT idpoint FROM linepoint);

-- check duplikasi point
SELECT idpoint, count(*) c FROM point GROUP BY CONCAT(lat,'-',lng) having c > 1;

-- check point punya lat lng tertentu
SELECT p.idpoint, lp.idline, l.name FROM point p
LEFT JOIN linepoint lp ON p.idpoint = lp.idpoint
LEFT JOIN line l ON lp.idline = l.idline
WHERE 
lat = (SELECT lat FROM point WHERE idpoint = '1687') AND 
lng = (SELECT lng FROM point WHERE idpoint = '1687');

-- point di temporary table
SELECT idline, sequence, stop, idinterchange,
    (SELECT pt.idpoint FROM point pt WHERE pt.lat = tAL.lat AND pt.lng = tAL.nlng 
		AND idpoint >= (SELECT idpoint FROM point WHERE idpoint NOT IN (SELECT idpoint FROM linepoint)) 
    ) AS idpoint
    FROM tAL;
    
 -- check orphan points  
SELECT idpoint FROM point WHERE idpoint NOT IN (SELECT idpoint FROM linepoint);

-- Geser-geser semua yang inbound

CREATE TEMPORARY TABLE geser AS (
SELECT p.idpoint
FROM linepoint lp
LEFT JOIN line l ON l.idline = lp.idline
LEFT JOIN point p ON p.idpoint = lp.idpoint
WHERE l.direction = 'I'
);
UPDATE point SET lng = lng-0.00005 WHERE idpoint IN (SELECT idpoint FROM geser);
DROP TEMPORARY TABLE geser;

-- geser point tertentu

UPDATE point SET lng = lng - 0.000095
WHERE idpoint IN
(
	SELECT idpoint FROM linepoint 
	WHERE idline = '' AND sequence BETWEEN
	( 
	SELECT sequence from linepoint lp
	WHERE idpoint = '5206'
	) AND (
	SELECT sequence from linepoint lp
	WHERE idpoint = '5254'
	)
	ORDER BY sequence
);

-- geser semua point 

UPDATE point SET lng = lng - 0.00004
WHERE idpoint IN
(
	SELECT idpoint FROM linepoint 
	WHERE idline = '19'
);

-- Line yang sudah ada jalurnya

SELECT l.idline, l.name, count(lp.idpoint) AS points
FROM line l 
LEFT JOIN linepoint lp ON l.idline = lp.idline
GROUP BY l.idline 
HAVING count(lp.idpoint) > 0
ORDER BY l.name;


-- Delete point from interchange
UPDATE linepoint SET idinterchange = NULL WHERE idpoint = '5808';